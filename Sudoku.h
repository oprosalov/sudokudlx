#ifndef SUDOKU_H
#define SUDOKU_H

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <cstdlib>

// Declaration for use in Node struct.
struct Header;

struct Node
{
    Header* header;
    Node* up;
    Node* down;
    Node* left;
    Node* right;
    int IDrow;
};

struct Header : Node
{
    Header* leftHeader;
    Header* rightHeader;
    int size, IDcol;
};

class Sudoku
{
  private:
    int maxCol;
    int maxRow;
    bool isSearchFinished;

    Header* root;
    std::vector<std::vector<Node*>> matrix; //[324][729], [constraints][options]
    std::vector<Header*> header;
    std::vector<std::vector<bool>> matrixData;
    std::vector<int> solutionRows;


  public:
    Sudoku();
    ~Sudoku();
    void buildMatrixData();
    void buildMatrix();
    void search();
    void cover(Header* node);
    void uncover(Header* node);
    void printSolution();
    void addSolutionRow(Node*);     // Insert a custom solution row.
    void loadPuzzle(std::ifstream& file);
    void removeSolutionRow(Node*);
    int getRowIndex(int, int, int);
    Node* getFirstNodeInRow(int);
    Header* getNextCol();
};




#endif // SUDOKU_H

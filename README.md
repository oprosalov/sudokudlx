# Sudoku Dancing Links Solver

This is my implementation of a Sudoku puzzle solver using Knuth's Dancing Links algorithm. The code could use some housekeeping but it works well.

To use it, put a puzzle .txt file in the same folder as the executable and run from command line, either without a parameter or with a puzzle file name parameter. The file should have 9 lines of 9 numbers, with 0 in place of missing numbers. Sample files are provided.
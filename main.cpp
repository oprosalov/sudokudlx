#include "Sudoku.h"

#include <iostream>
#include <string>
#include <algorithm>
#include <fstream>
#include <ctime>

using namespace std;

void openFile(ifstream& file);

int main(int argc, char* argv[])
{
    // Usage check.
    if(argc != 1 && argc != 2)
        cerr << "USAGE: " << argv[0] << " PUZZLE_FILE(optional)\n";

    cout << endl << "Welcome to Oleg's DLX Sudoku Solver!" << endl << endl;
    string input;
    ifstream file;

    if(argc == 1)
        openFile(file);
    else if(argc == 2) {
        file.open(argv[1]);

        if(!file.is_open()){
            cout << "File not found!" << endl;
            openFile(file);
        }
    }

    clock_t start, end;
    start = clock();

    Sudoku puzzle;
    puzzle.loadPuzzle(file);
    //cout << endl << "success" << endl;
    puzzle.search();

    end = clock();

    cout << endl << "Elapsed Time: " << (end - start) / (double)(CLOCKS_PER_SEC / 1000) << "ms" << endl;

    return 0;
}

void openFile(ifstream& file)
{
    string fileName;
    do{
        cout << "Enter puzzle file name: ";
        cin >> fileName;
        file.open(fileName.c_str());

        if(!file.is_open()){
            file.close();
            cout << "File not found!" << endl;
        }
    } while(!file.is_open());
}




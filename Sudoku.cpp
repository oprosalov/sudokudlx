#include "Sudoku.h"
using namespace std;

Sudoku::Sudoku() : maxCol(324), maxRow(729),
                   matrix(maxCol, vector<Node*>(maxRow)),
                   matrixData(maxCol, vector<bool>(maxRow, false)),
                   header(maxCol), isSearchFinished(false)
{
    // Init root, node matrix, and header vector.
    root = new Header;

    for(int k = 0; k < maxCol; k++) {
        header[k] = new Header;

        for(int r = 0; r < maxRow; r++) {
            matrix[k][r] = new Node;
        }
    }

    // Init constraint table.
    buildMatrixData();

    // Init toroidally linked matrix
    buildMatrix();

    // temp
    //addSolutionRow(getFirstNodeInRow(getRowIndex(3, 1, 1)));
//    addSolutionRow(getFirstNodeInRow(getRowIndex(3, 1, 2)));
//    addSolutionRow(getFirstNodeInRow(getRowIndex(6, 1, 3)));
//    addSolutionRow(getFirstNodeInRow(getRowIndex(7, 1, 4)));
//    addSolutionRow(getFirstNodeInRow(getRowIndex(4, 1, 5)));
//    addSolutionRow(getFirstNodeInRow(getRowIndex(1, 1, 6)));
//    addSolutionRow(getFirstNodeInRow(getRowIndex(2, 1, 7)));
//    addSolutionRow(getFirstNodeInRow(getRowIndex(8, 1, 8)));
//    addSolutionRow(getFirstNodeInRow(getRowIndex(5, 1, 9)));
//    addSolutionRow(getFirstNodeInRow(getRowIndex(4, 2, 2)));
//    addSolutionRow(getFirstNodeInRow(getRowIndex(9, 9, 8)));
//    addSolutionRow(getFirstNodeInRow(getRowIndex(7, 9, 9)));
    //solutionRows.push_back(0);
}

Sudoku::~Sudoku()
{
    for(int k = 0; k < maxCol; k++) {
        delete header[k];

        for(int r = 0; r < maxRow; r++) {
            delete matrix[k][r];
        }
    }
    delete root;
}

void Sudoku::buildMatrixData()
{
    // Initialize vector of Sudoku constraints
    int cellOffset, rowOffset, colOffset, sqOffset;

    for (int k = 0; k < maxCol; k++) {
        for (int r = 0; r < 9; r++) {       // This only needs to loop 9 times because there are only 9 assignments per col. The beauty of modular arithmetic.
            // Cell constraints (only one of value in each of 81 cells).
            if (k < (81)) {
                cellOffset = (k * 9) + r;
                matrixData[k][cellOffset] = true;
            }
            // Row constraints (only one of 1-9 in each of 9 rows).
            else if (k < (81 * 2)){
                rowOffset = ((k % 9) + (r * 9)) + (((k - 81) / 9) * 81);
                matrixData[k][rowOffset] = true;                                            // Please fix this horror asap.
            }
            // Column constraints (only one of 1-9 in each of 9 columns).
            else if (k < (81 * 3)){
                colOffset = (r * 81) + (k % 81);
                matrixData[k][colOffset] = true;
            }
            // 3x3 square constraints (only one of 1-9 in each of 3x3 squares).
            else {
                sqOffset = (r * 9) + (k % 9) + ((r / 3) * 54) + (((k - 243) / 9) * 27) + (((k - 243) / 27) * 162);
                matrixData[k][sqOffset] = true;
            }
        }
    }

//    Debug modular arithmetic. Compare with http://www.stolaf.edu/people/hansonr/sudoku/exactcovermatrix.htm.
//
//    ofstream file("matrix.txt");
//
//    for (int k = 0; k < 324; k++) {
//        file << "k = " << k << endl;
//        for (int r = 0; r < 9; r++) {//
//            if (k < (81)){
//                int cellOffset = (k * 9) + r;
//                file << "r" << (cellOffset / 81) << "c" << (cellOffset / 9) % 9 << "#" << cellOffset % 9 << endl;
//            }
//            else if (k < (81 * 2)){
//                int rowOffset = ((k % 9) + (r * 9)) + (((k - 81) / 9) * 81);
//                file << "r" << rowOffset / 81 << "c" << (rowOffset / 9) % 9 << "#" << rowOffset % 9 << endl;
//            }
//            else if (k < (81 * 3)){
//                int colOffset = (r * 81) + (k % 81);
//                file << "r" << colOffset / 81 << "c" << (colOffset / 9) % 9 << "#" << colOffset % 9 << endl;
//            }
//            else {
//                int sqOffset = (r * 9) + (k % 9) + ((r / 3) * 54) + (((k - 243) / 9) * 27) + (((k - 243) / 27) * 162);
//                file << "r" << sqOffset / 81 << "c" << (sqOffset / 9) % 9 << "#" << sqOffset % 9 << endl;
//            }
//        }
//    }
}

void Sudoku::buildMatrix()
{
    // Convert vector to a toroidally linked matrix.
    int i, j;
    for (int k = 0; k < maxCol; k++) {
        // Column headers left and right
        if (k == 0) {
            header[k]->leftHeader = root;
            header[k]->rightHeader = header[k + 1];
        }
        else if (k == maxCol - 1) {
            header[k]->leftHeader = header[k - 1];
            header[k]->rightHeader = root;
        }
        else {
            header[k]->leftHeader = header[k - 1];
            header[k]->rightHeader = header[k + 1];
        }

        header[k]->IDcol = k;
        //header[k]->IDrow = -1;

        for (int r = 0; r < maxRow; r++) {
            //matrix[k][r]->IDrow = -1; // used for debug print of matrix
            if (matrixData[k][r] == true) {
                // Left
                i = k; j = r;
                do {
                    (i - 1 < 0) ? i = maxCol - 1 : i--;
                } while (matrixData[i][j] != true);
                matrix[k][r]->left = matrix[i][j];

                // Right
                i = k; j = r;
                do {
                    i = (i + 1) % maxCol;
                } while (matrixData[i][j] != true);
                matrix[k][r]->right = matrix[i][j];

                // Up and header down
                i = k; j = r;
                do {
                    if ((j - 1) < 0) {
                        matrix[k][r]->up = header[k];
                        header[k]->down = matrix[k][r];
                        break;
                    }
                    j--;
                    if (matrixData[i][j]) {
                        matrix[k][r]->up = matrix[i][j];
                        break;
                    }
                } while (matrixData[i][j] != true);

                // Down and header up
                i = k; j = r;
                do {
                    if (j + 1 == maxRow) {
                        matrix[k][r]->down = header[k];
                        header[k]->up = matrix[k][r];
                        break;
                    }
                    j = (j + 1) % maxRow;
                    if (matrixData[i][j]) {
                        matrix[k][r]->down = matrix[i][j];
                        break;
                    }
                } while (j < maxRow);

                // Header pointer and properties
                matrix[k][r]->header = header[k];
                header[k]->size += 1;
                header[k]->IDcol = k;
                header[k]->header = header[k];  // Headers point to themselves for use in cover() and uncover().

                matrix[k][r]->IDrow = r;
            }
        }
    }

    // root
    root->leftHeader = header[maxCol - 1];
    root->rightHeader = header[0];
    root->IDcol = -1;

    //cout << root << " " << header[maxCol - 1]->rightHeader << " " << root->leftHeader << " " << header[maxCol - 1] << endl;

//    for (int i = 0; i < maxCol; i++) {
//        cout << "header col: " << header[i].IDcol << endl
//             << "left: " << header[i].leftHeader->IDcol << endl
//             << "right: " << header[i].rightHeader->IDcol << endl
//             << "up: " << header[i].up->IDrow << endl
//             << "down: " << header[i].down->IDrow << endl << endl;
//    }
//
//
//
//    cout << "header[0].down = " << header[0].down->IDrow << endl;
    //cout << "header[0].down->down = " << header[0].down->down->IDrow << endl;

//    Node one;
//    Node* p = &one;
//    cout << &p

//    Header* temp;
//    for (int i = 0; i < maxCol; i++) {
//        cout << "i = " << i << " -> ";
//        temp = static_cast<Header*>(header[i].left);
//        cout << "left = " << temp->IDcol << " ";
//
//        temp = static_cast<Header*>(header[i].right);
//        cout << "right = " << temp->IDcol << endl << endl;
//    }



// Header debug
//    for (int r = 0; r < maxCol; r++) {
//        cout << header[r].IDcol << " = " << header[r].size << endl;
//    }

//// Debug print matrix.
//    for (int k = 0; k < maxRow; k++) {
//        if (k % 9 == 0) cout << endl;
//        for (int r = 0; r < maxCol; r++) {
//            //if (k == -1) cout << header[r].IDcol;
//            //else
//            //matrix[r][k]->IDrow > -1 ? cout << matrix[r][k]->header : cout << " ";
//            matrix[r][k]->IDrow > -1 ? cout << "X" : cout << " ";
//
//        }
//        cout << endl;
//    }
}

void Sudoku::search()
{
    if ((root->rightHeader == root)) {
        printSolution();
        isSearchFinished = true;
        return;
    }
    else {
        Header* col = getNextCol();
        cover(col);

        for (Node* row = col->down; row != col && !isSearchFinished; row = row->down) {
            solutionRows.push_back(row->IDrow);

            for (Node* rightNode = row->right; rightNode != row; rightNode = rightNode->right)
                cover(rightNode->header);

            search();
            removeSolutionRow(row);

            for (Node* leftNode = row->left; leftNode != row; leftNode = leftNode->left)
                uncover(leftNode->header);
        }
        uncover(col);
    }
}

void Sudoku::cover(Header* header)
{
    header->rightHeader->leftHeader = header->leftHeader;
    header->leftHeader->rightHeader = header->rightHeader;

    for (Node* row = header->down; row != header; row = row->down) {
        for (Node* rightNode = row->right; rightNode != row; rightNode = rightNode->right) {
            rightNode->up->down = rightNode->down;
            rightNode->down->up = rightNode->up;
            rightNode->header->size--;
        }
    }
}

void Sudoku::uncover(Header* header)
{
    for (Node* row = header->up; row != header; row = row->up) {
        for (Node* leftNode = row->left; leftNode != row; leftNode = leftNode->left) {
            leftNode->up->down = leftNode;
            leftNode->down->up = leftNode;
            leftNode->header->size++;
        }
    }
    header->rightHeader->leftHeader = header;
    header->leftHeader->rightHeader = header;
}


int Sudoku::getRowIndex(int num, int row, int col)
{
    num--, row--, col--;
    return num + (row * 81) + (col * 9);
}

Node* Sudoku::getFirstNodeInRow(int rowIndex)
{
    for(int i = 0; i < maxCol; i++) {
        if(matrix[i][rowIndex]->IDrow == rowIndex)
            return matrix[i][rowIndex];
    }
}


void Sudoku::addSolutionRow(Node* node)
{
    // Insert a custom solution row.

    cover(node->header);
    solutionRows.push_back(node->IDrow);

    for (Node* rightNode = node->right; rightNode != node; rightNode = rightNode->right) {
        cover(rightNode->header);
    }
}

void Sudoku::loadPuzzle(ifstream& file)
{
    string line;//, data;
    char data;
    int num;
    stringstream iss;

    for(int x = 1; getline(file, line) && x <= 9; x++){
        //cout << "--x loop: y = " << y << endl;
        //cout << endl;
        iss.clear();
        iss << line;
        //cout << x << " |line = " << line << endl;

        for(int y = 1; iss.get(data) && y <= 9; y++){
            //cout << "y loop: y = " << y << endl;
            //file >> noskipws >> num;
            //cout << num;

//            stringstream convert(data);
//            convert >> num;
            num = data - '0';

            if(num != 0) {
                //cout << "num = " << num << " x = " << x << " y = " << y << endl;
                addSolutionRow(getFirstNodeInRow(getRowIndex(num, x, y)));
            }

//            if(num != ' ' && num != '\n' && num != '0')
//                cout << "num = " << num - '0' << " x = " << x << " y = " << y << endl;
//                addSolutionRow(getFirstNodeInRow(getRowIndex(num - '0', x, y)));
        }
    }
}

void Sudoku::removeSolutionRow(Node* node)
{
    vector<int>::iterator it;

    for(it = solutionRows.begin(); it != solutionRows.end(); it++){
        if (*it == node->IDrow){
            solutionRows.erase(it);
            break;
        }
    }
}


void Sudoku::printSolution()
{
    int board[9][9];
    int r, k, n;

    for (int i = 0; i < 81; i++) {
        r = solutionRows[i] / 81;
        k = (solutionRows[i] / 9) % 9;
        n = (solutionRows[i] % 9) + 1;
        board[r][k] = n;
    }

    for(int i = 0; i < 9; i++){
        if(i % 3 == 0)
            cout << "--------------------" << endl;
        for(int j = 0; j < 9; j++){
            if(j % 3 == 0)
                cout << "|";

            cout << board[i][j] << " ";
        }
        cout << endl;
    }
}



Header* Sudoku::getNextCol()
{
    // Gets smallest col to improve performance. Source: https://en.wikipedia.org/wiki/Dancing_Links#Header

    Header* smallestCol;
    int tempSize = 10;      // Initial comparison, size can be at most 9.

    for (Header* nextCol = root->rightHeader; nextCol->rightHeader != root; nextCol = nextCol->rightHeader) {
        if (nextCol->size < tempSize) {
            smallestCol = nextCol;
            tempSize = smallestCol->size;
        }
    }

    return smallestCol;
}







